package com.example.testandroid

import android.Manifest
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.content.pm.PackageManager
import android.hardware.*
import android.os.Bundle
import android.hardware.camera2.CameraManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.widget.ImageView
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

class MainActivity : AppCompatActivity(), LocationListener, SensorEventListener{

    private val objCameraManager: CameraManager? = null
    private val mCameraId: String? = null
    private var isONOFF: Switch? = null
    private var isTorchOn: Boolean = false
    private var manager: CameraManager? = null
    private var m_Camera: Camera? = null
    private var test_view_location: TextView? = null
    private var sensorManager: SensorManager? = null
    private var compass_image: ImageView? = null
    private var text_view_degree: TextView? = null
    private var globalLocation: Location? = null

    lateinit var mapFragment: SupportMapFragment
    lateinit var googleMap: GoogleMap

    val REQUEST_LOCATION = 2


    private fun convertLocationToString(latitude: Double, longitude: Double): String{
        val builder = StringBuilder()
        if(latitude < 0){
            builder.append("S ")
        }else{
            builder.append("N ")
        }
        val latitudeDegrees = Location.convert(Math.abs(latitude), Location.FORMAT_SECONDS)
        val latitudeSplit = latitudeDegrees.split((":").toRegex()).dropLastWhile({it.isEmpty()}).toTypedArray()
        builder.append(latitudeSplit[0])
        builder.append("'")
        builder.append(latitudeSplit[1])
        builder.append("'")
        builder.append(latitudeSplit[2])
        builder.append("/")
        builder.append("\n")

        if(longitude < 0){
            builder.append("W ")
        }else{
            builder.append("E ")
        }

        val longitudeDegrees = Location.convert(Math.abs(longitude), Location.FORMAT_SECONDS)
        val longitudeSplit = longitudeDegrees.split((":").toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
        builder.append(longitudeSplit[0])
        builder.append("'")
        builder.append(longitudeSplit[1])
        builder.append("'")
        builder.append(longitudeSplit[2])
        builder.append("/")
        builder.append("\n")

        return builder.toString()
    }

    private fun setLocation(){
        if(ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ){
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION),
                REQUEST_LOCATION)
        }else{
            val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val criteria = Criteria()
            val provider = locationManager.getBestProvider(criteria, false)
            val location = locationManager.getLastKnownLocation(provider)
            globalLocation = locationManager.getLastKnownLocation(provider)
            println(globalLocation!!.longitude)

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000,0f,this)
            if(location != null){
            test_view_location!!.text = convertLocationToString(location.latitude, location.longitude)
            }else{
                Toast.makeText(this,"Location not availiable!",Toast.LENGTH_SHORT).show()
            }

        }

    }


    override fun onLocationChanged(p0: Location?) {
        setLocation()
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {

    }

    override fun onProviderEnabled(p0: String?) {

    }

    override fun onProviderDisabled(p0: String?) {

    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }

    private var rotationMatrix = FloatArray(9)
    private var orientation = FloatArray(3)
    private var azimuth: Int = 0
    private var lastAccelerometer = FloatArray(3)
    private var lastAccelerometerSet = false
    private var lastMagnetometer = FloatArray(3)
    private var lastMagnetometerSet = false
    override fun onSensorChanged(event: SensorEvent?) {
        if(event!!.sensor.type == Sensor.TYPE_ROTATION_VECTOR){
            SensorManager.getRotationMatrixFromVector(rotationMatrix, event.values)
            azimuth = (Math.toDegrees(SensorManager.getOrientation(rotationMatrix, orientation)[0].toDouble())+360).toInt()%360
        }

        if(event.sensor.type == Sensor.TYPE_ACCELEROMETER){
            System.arraycopy(event.values,0,lastAccelerometer,0,event.values.size)
            lastAccelerometerSet = true
        }else if(event.sensor.type == Sensor.TYPE_MAGNETIC_FIELD){
            System.arraycopy(event.values,0,lastMagnetometer,0,event.values.size)
            lastMagnetometerSet = true
        }
        if(lastMagnetometerSet && lastAccelerometerSet){
            SensorManager.getRotationMatrix(rotationMatrix, null,lastAccelerometer,lastMagnetometer)
            SensorManager.getOrientation(rotationMatrix, orientation)
            azimuth = (Math.toDegrees(SensorManager.getOrientation(rotationMatrix,orientation)[0].toDouble())+360).toInt()%360
        }

        azimuth = Math.round(azimuth.toFloat())
        compass_image!!.rotation = (-azimuth).toFloat()

        val where = when(azimuth){
            in 281..349 -> "NW"
            in 261..280 -> "W"
            in 191..260 -> "SW"
            in 171..190 -> "S"
            in 101..170 -> "SE"
            in 81..100  -> "E"
            in 11..80   -> "NE"
            else -> "N"
        }

       //text_view_degree!!.text = "$azimuth $where"



    }

    private var accelerometer: Sensor? = null
    private var magnetometer: Sensor? = null
    private var rotationVector: Sensor? = null

    private var haveSensorAccelerometer = false
    private var haveSensorMagnetometer = false
    private var haveSensorRotationVector = false


    fun startCompass(){
      if(sensorManager!!.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) == null){
          if(sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) == null ||
              sensorManager!!.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) == null){
          noSensorAlert()
          }else{
              accelerometer = sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
              magnetometer = sensorManager!!.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)

              haveSensorAccelerometer = sensorManager!!.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI)
              haveSensorMagnetometer = sensorManager!!.registerListener(this,magnetometer, SensorManager.SENSOR_DELAY_UI)
          }
      }else{
          rotationVector = sensorManager!!.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
          haveSensorRotationVector = sensorManager!!.registerListener(this,rotationVector ,SensorManager.SENSOR_DELAY_UI)
      }
    }

    private fun stopCompass(){
        if(haveSensorRotationVector){
            sensorManager!!.unregisterListener(this, rotationVector)
        }
        if(haveSensorAccelerometer){
            sensorManager!!.unregisterListener(this,accelerometer)
        }
        if(haveSensorMagnetometer){
            sensorManager!!.unregisterListener(this,magnetometer)
        }
    }

    private fun noSensorAlert(){
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setMessage("Votre appareil ne supporte pas la boussole").setCancelable(false).setNegativeButton("Fermer"){_,_ -> finish()}
        alertDialog.show()
    }

    override fun onResume() {
        super.onResume()
        startCompass()
    }

    override fun onPause(){
        super.onPause()
        stopCompass()
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(OnMapReadyCallback {
            googleMap = it
            val location1 = LatLng(globalLocation!!.latitude, globalLocation!!.longitude)
            googleMap.addMarker(MarkerOptions().position(location1).title("My Location"))
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location1,10f))
        })


        // je verifie que le flash est bien utilisable sur mon appareil
        val isFlashAvailable = applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
        isONOFF = findViewById(R.id.onOffFlashlight) as Switch
        test_view_location = findViewById(R.id.test_view_location) as TextView
        compass_image = findViewById(R.id.compass_image) as ImageView
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        //text_view_degree = findViewById(R.id.text_view_degree) as TextView
        setLocation()
        manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager

        isONOFF!!.setOnClickListener {
            if (isTorchOn) {
                flashLightSwitch(true)
                //turn false
                isTorchOn = false
            } else {
                flashLightSwitch(false)
                //turn true
                isTorchOn = true
            }
        }



    }
    private fun flashLightSwitch(flashState: Boolean) {
        if(flashState){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                manager!!.setTorchMode("0", false)
            }else if(m_Camera!=null){
                m_Camera!!.stopPreview()
                m_Camera!!.release()
                m_Camera=null
            }
        }else{
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                manager!!.setTorchMode("0", true)
            }else{
                println("test")
                val pm = packageManager
                val features = pm.systemAvailableFeatures

                for(feature in features){
                    if(PackageManager.FEATURE_CAMERA_FLASH == feature.name){
                        if(null == m_Camera){
                            m_Camera = Camera.open()
                        }
                        var parameters = m_Camera!!.parameters
                        parameters.flashMode = Camera.Parameters.FLASH_MODE_TORCH
                        m_Camera!!.parameters = parameters
                        m_Camera!!.startPreview()
                    }
                }
            }
        }
    }
    companion object{
        //private var isOpen = false
        private val camera:android.hardware.Camera?=null
    }
}

